class CreateLinks < ActiveRecord::Migration[6.0]
  def change
    create_table :links do |t|
      t.references :user, null: false, foreign_key: true
      t.string :url
      t.integer :visits_count
      t.string :code

      t.timestamps
    end
  end
end
