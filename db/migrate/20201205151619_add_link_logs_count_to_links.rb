class AddLinkLogsCountToLinks < ActiveRecord::Migration[6.0]
  def change
    add_column :links, :link_logs_count, :integer
  end
end
