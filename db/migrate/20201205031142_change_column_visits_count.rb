class ChangeColumnVisitsCount < ActiveRecord::Migration[6.0]
  def change
      change_column :links, :visits_count, :integer, default: 0
  end
end
