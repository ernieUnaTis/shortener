# == Schema Information
#
# Table name: links
#
#  id           :integer          not null, primary key
#  user_id      :integer          not null
#  url          :string
#  visits_count :integer
#  code         :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
FactoryBot.define do
  factory :link do
    user
    url { FFaker::Internet.http_url }
    visits_count { 0 }
  end
end
