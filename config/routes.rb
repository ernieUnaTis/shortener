# frozen_string_literal: true

Rails.application.routes.draw do
  resources :links
  get 'landing/index'
  devise_for :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  get "link_stats/:id", to:"links#stats", as:"link_stats"

  root to: 'landing#index'

  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :links_api, only: [:index,:create]
    end
  end
end
