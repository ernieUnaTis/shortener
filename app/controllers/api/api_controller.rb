# frozen_string_literal: true
module Api
  class Api::ApiController < ActionController::Base
    before_action :authenticate
    skip_before_action :verify_authenticity_token
    include ErrorSerializer

    rescue_from ActionController::ParameterMissing do |exception|
      render json: { error: exception.message }, status: :bad_request
    end
    

    rescue_from ActiveRecord::RecordInvalid do |e|
      render serializer: ErrorSerializer,json: {error: {RecordInvalid: e.record.errors}}, status: 406
    end
  
    rescue_from ActiveRecord::RecordNotFound do
        render serializer: ErrorSerializer,json: {error: {RecordNotFound: "Record not found for id: #{params[:id]}"}}, status: 404    
    end 

    private
    def authenticate
      api_key = request.headers['X-Api-Key']
      @user = User.where(api_key: api_key).first if api_key
     
      unless @user
        head :unauthorized
        return false
      end
    end

    protected
    def self.set_pagination_headers(options = {})

        scope = @links 
        page = {}
        page[:first] = 1 if scope.total_pages > 1 && !scope.first_page?
        page[:last] = scope.total_pages  if scope.total_pages > 1 && !scope.last_page?
        page[:next] = scope.current_page + 1 unless scope.last_page?
        page[:prev] = scope.current_page - 1 unless scope.first_page?
    
        pagination_links = []
        page.each do |k, v|
          new_request_hash= request_params.merge({:page => v})
          pagination_links << "<#{url_without_params}?#{new_request_hash.to_param}>; rel=\"#{k}\""
        end
        headers["Link"] = pagination_links.join(", ")
     
    end
 
  
  end
end
