class Api::V1::LinksApiController <  Api::ApiController
    respond_to :json
    before_action :authenticate
   

    def index
        @links = @user.links.order('created_at DESC').page(params[:page] || 1).per(5)
        set_pagination_headers
        render json: LinkSerializer.new(@links), status: :ok
    end

    def create
        @link = @user.links.build(link_params)

        if @link.save
            render json: LinkSerializer.new(@link), status: :created
        else
            render json: ErrorSerializer.serialize(@link.errors), status: :bad_request
        end
    end

    private
    def link_params
        params
          .require(:link)
          .permit(:url)
    end

    def per_page
        (params[:per_page] || 5).to_i
    end

    def set_pagination_headers
        pc = @links
        headers["X-Total-Count"] = pc.total_count

        links = []
        links << page_link(1, "first") unless pc.first_page?
        links << page_link(pc.prev_page, "prev") if pc.prev_page
        links << page_link(pc.next_page, "next") if pc.next_page
        links << page_link(pc.total_pages, "last") unless pc.last_page?
        headers["Link"] = links.join(", ") if links.present?
    end

    def page_link(page, rel)
        base_uri = request.url.split("?").first
        "<#{base_uri}?#{request.query_parameters.merge(page: page).to_param}>; rel='#{rel}'"
    end 
  

end