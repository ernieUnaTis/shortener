class LinksController < ApplicationController
  before_action :set_link, only: [:edit, :update, :destroy]

  def index
    @links = current_user.links.order('created_at DESC').page(params[:page] || 1).per(5)
  end

  def show
    ip = request.remote_ip
		user_agent = request.user_agent
    @link = current_user.links.find_by(code: params[:id])
    @link.link_logs.create!(:ip=>ip,:user_agent=>user_agent)
    redirect_to @link.url
  end

  def new
    @link = Link.new
  end

  def edit
  end

  def create
    @link = current_user.links.build(link_params)

    if @link.save
      redirect_to links_path, notice: 'Link was successfully created.'
    else
      render :new
    end
  end

  def update
    respond_to do |format|
      if @link.update(link_params)
        format.html { redirect_to @link, notice: 'Link was successfully updated.' }
        format.json { render :show, status: :ok, location: @link }
      else
        format.html { render :edit }
        format.json { render json: @link.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @link.destroy
    respond_to do |format|
      format.html { redirect_to links_url, notice: 'Link was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def stats
    @link_logs = current_user.links.find(params[:id]).link_logs.group(:user_agent)
  end

  private

  def set_link
    @link = current_user.links.find(params[:id])
  end

  def link_params
  params
  .require(:link)
    .permit(:url)
  end
  
  def per_page
	 (params[:per_page] || 5).to_i
  end
  
end
