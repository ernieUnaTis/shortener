json.extract! link, :id, :user_id, :url, :visits_count, :code, :created_at, :updated_at
json.url link_url(link, format: :json)
