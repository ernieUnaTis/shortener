class LinkSerializer
  include FastJsonapi::ObjectSerializer
  attributes :url, :code, :visits_count
end
