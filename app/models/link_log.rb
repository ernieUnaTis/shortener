class LinkLog < ApplicationRecord
  belongs_to :link, counter_cache: true
  before_create :add_visitor

  def add_visitor
  	link.increment!(:visits_count)
  end


end
