# == Schema Information
#
# Table name: links
#
#  id           :integer          not null, primary key
#  user_id      :integer          not null
#  url          :string
#  visits_count :integer
#  code         :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
class Link < ApplicationRecord
  belongs_to :user, counter_cache: true
  has_many :link_logs
  validates :url, format: { with: URI::regexp(%w(http https)), message: 'You provided invalid URL' }

  before_create :generate_code

  def generate_code
    self.code = SecureRandom.hex(3)
  end
end
