# == Schema Information
#
# Table name: links
#
#  id           :integer          not null, primary key
#  user_id      :integer          not null
#  url          :string
#  visits_count :integer
#  code         :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
require 'rails_helper'

RSpec.describe Link, type: :model do
  subject { FactoryBot.build(:link) }

  describe 'attributes' do
    it { should respond_to(:url) }
    it { should respond_to(:visits_count) }
    it { expect(subject.visits_count).to eq(0) }
  end

  describe 'associations' do
    it { should belong_to(:user) }
  end

  describe 'Save method' do
    it 'Is expected to have an assigned code' do
      subject.save
      expect(subject.code.blank?).to be_falsy
      # Esperas que code sea una string.
      expect(subject.code.is_a?(String)).to be_truthy
    end
  end
end
